﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD_Lemeur_Garcia
{
    class MainViewModel
    {
        public List<Musicien> Compositeurs { get; set; }
        public List<Pays> Pays { get; set; }

        public List<Album> Albums { get; set; }
        public List<Album> Empruntés { get; set; }

        public MusiqueEntities m;
        public MainViewModel()
        {
            m = new MusiqueEntities();
            Compositeurs = (m.Musicien.Where(c => c.Oeuvre.Count()>0).ToList());

            Pays = m.Pays.OrderBy(c => c.Nom_Pays).ToList();

            Albums = m.Album.OrderBy(a => a.Titre_Album).ToList();
            Empruntés = new List<Album>();
           
          
         
        }




    }
}
