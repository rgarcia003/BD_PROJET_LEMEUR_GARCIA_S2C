﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BD_Lemeur_Garcia
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    /// 
    public partial class Musicien
    {
        public override string ToString()
        {
            return Nom_Musicien + "-" + Prénom_Musicien;
        }
    }

    public partial class Album
    {
        public override string ToString()
        {
            return Titre_Album;
        }
    }
    public partial class MainWindow : Window
    {
        MainViewModel mvm;

        public MainWindow()
        {
                    
            InitializeComponent();
            mvm = new MainViewModel();
            DataContext = mvm;
            recherche.TextChanged += recherche_TextChanged;
            recherche2.TextChanged += recherche2_TextChanged;
            reserver.MouseDown+= reserver_Click;
            rendre.MouseDown += rendre_Click;
            panier.MouseDown += panier_Click;
            creer.MouseDown += creer_Click;

        }



        private void recherche_TextChanged(object sender, TextChangedEventArgs e)
        {
          mvm.Compositeurs.Clear();
         
            List<Musicien> filtrés = (mvm.m.Musicien.Where(p => p.Nom_Musicien.StartsWith(recherche.Text)).ToList());
            foreach (Musicien m in filtrés)
                mvm.Compositeurs.Add(m);
         
            listeMusiciens.Items.Refresh();
        }

        private void recherche2_TextChanged(object sender, TextChangedEventArgs e)
        {
            mvm.Albums.Clear();

            List<Album> filtrés = (mvm.m.Album.Where(t => t.Titre_Album.StartsWith(recherche2.Text)).ToList());
            foreach (Album a in filtrés)
                mvm.Albums.Add(a);

            listeAlbums.Items.Refresh();
        }

        private void creer_Click(object o, RoutedEventArgs e)
        {

            var abo = (mvm.m.Abonné.Where(a => a.Nom_Abonné == nomabo.Text && a.Prénom_Abonné == prenomabo.Text)).ToList();
            //si l'abonné n'existe pas on le cree
            if (nomabo.Text.Length>0 && prenom.Text.Length>0 && abo.Count==0)
            {
                mvm.m.Abonné.Add(new Abonné { Nom_Abonné = nomabo.Text, Prénom_Abonné = prenomabo.Text, Login = nomabo.Text, Password = prenomabo.Text });
                mvm.m.SaveChangesAsync();

            }
    

        }

        private void reserver_Click(object o, RoutedEventArgs e)
        {
            MusiqueEntities context = new MusiqueEntities();

            var abo = (context.Abonné.Where(a => a.Nom_Abonné == nomabo.Text && a.Prénom_Abonné == prenomabo.Text)).ToList();

            // on testera si l'album est reserve
            Album courant = (Album)listeAlbums.SelectedItem;
            var reserve = (from ab in context.Abonné
                           join em in context.Emprunter on ab.Code_Abonné equals em.Code_Abonné
                           join al in context.Album on em.Code_Album equals al.Code_Album

                           where al.Code_Album==courant.Code_Album
                           select em).Distinct().ToList();
          
            if (courant != null)
            {
                if (reserve.Count>0)
                {
                    mvm.Empruntés.Clear();
                    mvm.Empruntés.Add(new Album { Titre_Album = "DEJA RESERVE \n Retour le: " + reserve.First().Date_Retour.ToString() });
                    listereservations.Items.Refresh();

                }//s'il ne l'est pas on l'emprunte pour une semaine
                else if (reserve.Count==0 && abo.Any())
                {
                    context.Emprunter.Add(new Emprunter { Code_Abonné = abo.First().Code_Abonné, Code_Album = courant.Code_Album, Date_Emprunt = DateTime.Now, Date_Retour = DateTime.Now.AddDays(7) });
                }
                context.SaveChangesAsync();
            }

        }
        

        private void rendre_Click(object o, RoutedEventArgs e)
        {
            MusiqueEntities context = new MusiqueEntities();
            Album courant = (Album)listereservations.SelectedItem;
            if (courant != null)
            {


                int todeleteabo = (from em in context.Emprunter
                                   join a in context.Abonné on em.Code_Abonné equals a.Code_Abonné
                                   where (a.Nom_Abonné == nomabo.Text) && (a.Prénom_Abonné == prenomabo.Text)
                                   select em.Code_Abonné).Distinct().FirstOrDefault();

                var empruntertodelete = (from a in context.Abonné
                                         join em in context.Emprunter on a.Code_Abonné equals em.Code_Abonné
                                         join al in context.Album on em.Code_Album equals al.Code_Album
                                         where (em.Code_Abonné==todeleteabo) && em.Code_Album==courant.Code_Album
                                         select em).Distinct().First();
                       

                context.Emprunter.Remove(empruntertodelete);

                context.SaveChangesAsync();
            }
        }

        private void panier_Click(object o, RoutedEventArgs e)
        {
          
            mvm.Empruntés.Clear();
     
            var empruntés= (from ab in mvm.m.Abonné
                                join em in mvm.m.Emprunter on ab.Code_Abonné equals em.Code_Abonné
                                join al in mvm.m.Album on em.Code_Album equals al.Code_Album
                                where (ab.Nom_Abonné == nomabo.Text) && (ab.Prénom_Abonné == prenomabo.Text)
                                select al).Distinct();
            
          

            foreach (Album a in empruntés)
                mvm.Empruntés.Add(a);

            listereservations.Items.Refresh();
        }

    }
}
