﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD_PROJET_FORM
{
    class Abonne
    {
         string m_nom, m_prenom;
     

        public Abonne(string nom, string prenom)
        {
            m_nom = nom;
            m_prenom = prenom;


        }
 
        public string Nom
        {
            get { return m_nom; }
          
        }
        public string Prénom
        {
            get { return m_prenom; }
         
        }
        public override string ToString()
        {
            return m_nom + " " + m_prenom;
        }
    }
}
