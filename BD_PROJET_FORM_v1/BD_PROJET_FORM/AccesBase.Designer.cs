﻿namespace BD_PROJET_FORM
{
    partial class AccesBase
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.Musiciens = new System.Windows.Forms.ListBox();
            this.Albums = new System.Windows.Forms.ListBox();
            this.search = new System.Windows.Forms.TextBox();
            this.nomAbo = new System.Windows.Forms.TextBox();
            this.prenomAbo = new System.Windows.Forms.TextBox();
            this.Reservations = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Reserver = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Rendre = new System.Windows.Forms.Button();
            this.Panier = new System.Windows.Forms.Button();
            this.commandes = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // Musiciens
            // 
            this.Musiciens.FormattingEnabled = true;
            this.Musiciens.Location = new System.Drawing.Point(12, 78);
            this.Musiciens.Name = "Musiciens";
            this.Musiciens.Size = new System.Drawing.Size(239, 316);
            this.Musiciens.TabIndex = 0;
            this.Musiciens.SelectedIndexChanged += new System.EventHandler(this.Musiciens_SelectedIndexChanged);
            // 
            // Albums
            // 
            this.Albums.FormattingEnabled = true;
            this.Albums.Location = new System.Drawing.Point(289, 78);
            this.Albums.Name = "Albums";
            this.Albums.Size = new System.Drawing.Size(249, 316);
            this.Albums.TabIndex = 1;
            this.Albums.SelectedIndexChanged += new System.EventHandler(this.Albums_SelectedIndexChanged);
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(12, 25);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(215, 20);
            this.search.TabIndex = 2;
            this.search.TextChanged += new System.EventHandler(this.Search_TextChanged);
            // 
            // nomAbo
            // 
            this.nomAbo.Location = new System.Drawing.Point(289, 25);
            this.nomAbo.Name = "nomAbo";
            this.nomAbo.Size = new System.Drawing.Size(100, 20);
            this.nomAbo.TabIndex = 3;
            // 
            // prenomAbo
            // 
            this.prenomAbo.Location = new System.Drawing.Point(409, 25);
            this.prenomAbo.Name = "prenomAbo";
            this.prenomAbo.Size = new System.Drawing.Size(100, 20);
            this.prenomAbo.TabIndex = 4;
            // 
            // Reservations
            // 
            this.Reservations.FormattingEnabled = true;
            this.Reservations.Location = new System.Drawing.Point(557, 192);
            this.Reservations.Name = "Reservations";
            this.Reservations.Size = new System.Drawing.Size(219, 95);
            this.Reservations.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Recherche";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(303, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom Abonne";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(416, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Prenom Abonne";
            // 
            // Reserver
            // 
            this.Reserver.Location = new System.Drawing.Point(617, 132);
            this.Reserver.Name = "Reserver";
            this.Reserver.Size = new System.Drawing.Size(94, 30);
            this.Reserver.TabIndex = 9;
            this.Reserver.Text = "Reserver";
            this.Reserver.UseVisualStyleBackColor = true;
            this.Reserver.Click += new System.EventHandler(this.Reserver_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Musiciens";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(384, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Albums";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(631, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Emprunteur";
            // 
            // Rendre
            // 
            this.Rendre.Location = new System.Drawing.Point(617, 312);
            this.Rendre.Name = "Rendre";
            this.Rendre.Size = new System.Drawing.Size(94, 30);
            this.Rendre.TabIndex = 13;
            this.Rendre.Text = "Rendre";
            this.Rendre.UseVisualStyleBackColor = true;
            this.Rendre.Click += new System.EventHandler(this.Rendre_Click);
            // 
            // Panier
            // 
            this.Panier.Location = new System.Drawing.Point(617, 417);
            this.Panier.Name = "Panier";
            this.Panier.Size = new System.Drawing.Size(94, 31);
            this.Panier.TabIndex = 14;
            this.Panier.Text = "Panier";
            this.Panier.UseVisualStyleBackColor = true;
            this.Panier.Click += new System.EventHandler(this.Panier_Click);
            // 
            // commandes
            // 
            this.commandes.FormattingEnabled = true;
            this.commandes.Location = new System.Drawing.Point(289, 400);
            this.commandes.Name = "commandes";
            this.commandes.Size = new System.Drawing.Size(249, 95);
            this.commandes.TabIndex = 15;
            // 
            // AccesBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 500);
            this.Controls.Add(this.commandes);
            this.Controls.Add(this.Panier);
            this.Controls.Add(this.Rendre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Reserver);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Reservations);
            this.Controls.Add(this.prenomAbo);
            this.Controls.Add(this.nomAbo);
            this.Controls.Add(this.search);
            this.Controls.Add(this.Albums);
            this.Controls.Add(this.Musiciens);
            this.Name = "AccesBase";
            this.Text = "AccesBase";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox Musiciens;
        private System.Windows.Forms.ListBox Albums;
        private System.Windows.Forms.TextBox search;
        private System.Windows.Forms.TextBox nomAbo;
        private System.Windows.Forms.TextBox prenomAbo;
        private System.Windows.Forms.ListBox Reservations;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Reserver;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Rendre;
        private System.Windows.Forms.Button Panier;
        private System.Windows.Forms.ListBox commandes;
    }
}

