﻿using BD_PROJET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD_PROJET_FORM
{
    public partial class AccesBase : Form
    {
        OleDbConnection dbConnection;
        public AccesBase()
        {
            InitializeComponent();

            string ChaineBd = "Provider=SQLOLEDB;Data Source=INFO-SIMPLET;Initial Catalog=Musique;Uid=ETD; Pwd=ETD";
            dbConnection = new OleDbConnection(ChaineBd);
            dbConnection.Open();

        }


        public void TrouveMusiciens(string initiale)
        {
      
            Musiciens.Items.Clear();
            string sql = "SELECT Code_Musicien,Nom_Musicien, Prénom_Musicien FROM Musicien WHERE Nom_Musicien Like '" + initiale + "%' ORDER BY Nom_Musicien ";
            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if (!reader.IsDBNull(2))
                {


                    Musicien musi = new Musicien(reader.GetInt32(0), reader.GetString(1), reader.GetString(2));

                    Musiciens.Items.Add(musi);
                }
                else
                {
                    Musicien musi = new Musicien(reader.GetInt32(0), reader.GetString(1),"");

                    Musiciens.Items.Add(musi);
                }
            }
            reader.Close();

            Console.Read();
        }
        public void chargerAlbums()
        {
                
            bool vide = true;
            Albums.Items.Clear();
            Musicien courant = (Musicien)Musiciens.SelectedItem;
            int id = courant.Code_Musicien;


            string sql="SELECT DISTINCT Album.Code_Album, titre_album FROM Album "+
			"INNER JOIN Disque ON Album.Code_Album = Disque.Code_Album "+
           " INNER JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque "+
			"INNER JOIN Enregistrement ON Composition_Disque.Code_Enregistrement = Enregistrement.Code_Enregistrement "+
			"INNER JOIN Composition ON Enregistrement.Code_Composition = Composition.Code_Composition "+
			"INNER JOIN Composition_Oeuvre ON Composition.Code_Composition = Composition_Oeuvre.Code_Composition "+
			"INNER JOIN Oeuvre ON Composition_Oeuvre.Code_Oeuvre = Oeuvre.Code_Oeuvre "+
			"INNER JOIN Composer ON Oeuvre.Code_Oeuvre = Composer.Code_Oeuvre "+
			"INNER JOIN Musicien ON Composer.Code_Musicien = Musicien.Code_Musicien "+
			"WHERE Composer.Code_Musicien ="+ id.ToString();

            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Album o = new Album(reader.GetInt32(0), reader.GetString(1), courant);
                Albums.Items.Add(o);
                vide = false;
            }
            if(vide)
            {
                Albums.Items.Add("N'a rien composé");
            }
            reader.Close();

        }

        public void chargerEmprunteurs()
        {
            bool vide = true;
            Reservations.Items.Clear();
            Album courant = (Album)Albums.SelectedItem;
            int courantcode = courant.getCode();
  
            string sql = "SELECT Nom_Abonné, Prénom_Abonné, Date_Retour FROM Abonné INNER JOIN Emprunter ON Abonné.Code_Abonné =Emprunter.Code_Abonné INNER JOIN Album ON Emprunter.Code_Album = Album.Code_Album WHERE Album.Code_Album =N'" + courantcode + "'";

            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Abonne ab = new Abonne(reader.GetString(0), reader.GetString(1));
                Reservations.Items.Add(ab);
                Reservations.Items.Add("Retour le: " + reader.GetDateTime(2).ToString());
                
                vide = false;
            }
            if (vide)
            {
               Reservations.Items.Add("N'est pas reserve");
            }
            reader.Close();
        }
        public OleDbConnection getConnection
      {
          get { return dbConnection; }
      }

        private void Musiciens_SelectedIndexChanged(object sender, EventArgs e)
        {
            chargerAlbums();
        }

        private void Search_TextChanged(object sender, EventArgs e)
        {
            TrouveMusiciens(search.Text);      
        }

        private void Reserver_Click(object sender, EventArgs e)
        {
            if ((Reservations.Items.Count == 0 || Reservations.Items.Contains("N'est pas reserve")) && Albums.Items.Count>0 && prenomAbo.Text.Length>0 && nomAbo.Text.Length>0)
            {
                string sql = "SELECT Nom_Abonné, Prénom_Abonné FROM Abonné WHERE Nom_Abonné=N'" + nomAbo.Text + "' AND Prénom_Abonné=N'" + prenomAbo.Text+"'";

                OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
                OleDbDataReader reader = cmd.ExecuteReader();
                if (!reader.Read())//si l'abonne n'existe pas on le cree
                {
                    string sql2 = "INSERT INTO Abonné(Nom_Abonné, Prénom_Abonné,Login,Password) VALUES ('" + nomAbo.Text + "','" + prenomAbo.Text + "','" + nomAbo.Text + "','" + prenomAbo.Text + "')";

                    OleDbCommand cmd2 = new OleDbCommand(sql2, dbConnection);
                    OleDbDataReader reader2 = cmd2.ExecuteReader();
                    reader2.Close();
                }
                
                //et on lui fait emprunter l'album pour une semaine
                string sql3 = "INSERT INTO Emprunter(Code_Abonné,Code_Album,Date_Emprunt,Date_Retour) VALUES ((SELECT Code_Abonné FROM Abonné WHERE Nom_Abonné=N'" + nomAbo.Text + "' AND Prénom_Abonné=N'" + prenomAbo.Text + "'), (SELECT Code_Album FROM Album WHERE Titre_Album =N'" + Albums.SelectedItem + "'),GETDATE(),GETDATE()+7)";
                OleDbCommand cmd3 = new OleDbCommand(sql3, dbConnection);
                OleDbDataReader reader3 = cmd3.ExecuteReader();
                chargerEmprunteurs();
                reader3.Close();
                reader.Close();
            }
            else if (prenomAbo.Text.Length > 0 && nomAbo.Text.Length > 0)
            {
                Reservations.Items.Clear();
                Reservations.Items.Add("DEJA RESERVE!");
            }
          
        }

        private void Rendre_Click(object sender, EventArgs e)
        {
            string sql = "DELETE FROM Emprunter WHERE Emprunter.Code_Abonné IN (SELECT Code_Abonné FROM Abonné WHERE Nom_Abonné=N'" + nomAbo.Text + "' AND Prénom_Abonné=N'" + prenomAbo.Text + "') AND Emprunter.Code_Album IN (SELECT Album.Code_Album FROM Album WHERE Titre_Album=N'"+Albums.SelectedItem+"')";

            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();
            chargerEmprunteurs();
            reader.Close();
        }

        private void viderExpirees()
        {

            string sql = "DELETE FROM Emprunter WHERE Date_Retour<GETDATE()";

            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();
           

            reader.Close();
        }
        private void Albums_SelectedIndexChanged(object sender, EventArgs e)
        {
            viderExpirees();
            chargerEmprunteurs();
        }

        private void Panier_Click(object sender, EventArgs e)
        {
            commandes.Items.Clear();
            string sql = "select distinct Titre_Album from Album inner join Emprunter on Emprunter.Code_Album = Album.Code_Album inner join Abonné on Abonné.Code_Abonné= Emprunter.Code_Abonné where Nom_Abonné=N'"+nomAbo.Text+"' and Prénom_Abonné =N'"+prenomAbo.Text+"'";

            OleDbCommand cmd = new OleDbCommand(sql, dbConnection);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
                commandes.Items.Add(reader.GetString(0));

            reader.Close();

        }


   


    }
}
