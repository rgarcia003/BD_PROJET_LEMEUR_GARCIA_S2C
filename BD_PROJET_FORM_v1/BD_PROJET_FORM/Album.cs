﻿using BD_PROJET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BD_PROJET_FORM
{
    class Album
    {
        int m_code;
        string m_titre;
   
        Musicien m_compo;

        public Album(int code,string titre, Musicien Compo)
        {
            m_code = code;
            m_titre = titre;
            m_compo = Compo;

        }

        public override string ToString()
        {
            return m_titre;
        }

        public int getCode()
        {
            return m_code;
        }


    }
}
