﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BD_PROJET_FORM;

namespace BD_PROJET
{
    public class Musicien
    {
        int code_Musicien;
        string nom_Musicien;
        string prénom_Musicien;
        public Musicien(int code, string nom, string prenom)
        {
            code_Musicien = code;
            prénom_Musicien = prenom;
            nom_Musicien = nom;
        }
        public int Code_Musicien
        {
            get { return code_Musicien; }
            set { code_Musicien = value; }
        }
        public string Nom_Musicien
        {
            get { return nom_Musicien; }
            set { nom_Musicien = value; }
        }
        public string Prénom_Musicien
        {
            get { return prénom_Musicien; }
            set { prénom_Musicien = value; }
        }
        public override string ToString()
        {
            return Nom_Musicien + " " + Prénom_Musicien;
        }

     
    }
}
           
           

            
 