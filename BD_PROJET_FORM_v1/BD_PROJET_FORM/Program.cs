﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BD_PROJET;


namespace BD_PROJET_FORM
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AccesBase Listes = new AccesBase();
            Listes.TrouveMusiciens("");
          

            Application.Run(Listes);
      
            Console.Read();
        }
    }
}

          


